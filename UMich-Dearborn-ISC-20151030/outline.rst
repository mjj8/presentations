
Slides outline
==============

 * why autonomous driving?
 * why is this hard?
 * who is working on this?
 * Toyota's AHDA
 * motion planning demo





Introduction
------------
 * Autonomous driving is a classic robotics problem brought into the realm of
   mass production.
 * Driving: 
    * Dull: stop-and-go traffic 
    * Dirty: pollution
    * Dangerous: vehicle related injuries and fatalities
   Flipped around, this is often expressed positively as:
    * Safety
    * Efficiency
    * Comfort

30,057 fatal motor vehicle crashes in which 32,719 deaths occurred
10.3 deaths per 100,000 people 
1.11 deaths per 100 million vehicle miles traveled



Current Technology
------------------
 * Research level
    * high precision sensors: lidar, cameras, gps, 
 * Mass production
    * low precision sensors: radar, cameras, gps
 * Maps: https://company.here.com/automotive/intelligent-car/hd-map/


Human driver:
visual sensing
haptic sensing  ==>  cognition  ==> actuation ==> physiological interaction
proprioception
inertial sensing


Toyota's Highway Driving Assist
-------------------------------
 * https://www.youtube.com/watch?v=NVxuY5rBvQg

 * production ready sensors and maps
 * lane level localization
 * driver monitoring
 * warning about upcoming challenging scenes
 * autonomous control to increase lateral and longitudinal safety margins



Experimenting with motion planning
-----------------------------


